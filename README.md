# Wordpress Text Widget #

Replaces the built in text widget with one that includes advanced options to apply a CSS class, add paragraphs, apply shortcodes and hide the title.

## Usage ##

Upload to plugins directory and activate on Wordpress plugin page. Drag on to any active sidebar on the Appearance > Widgets page.

Click on the 'Show Advanced' link in the widget to change advanced options.

### Add CSS Class(es) ###

Strings entered here, either space or comma seperated, will be applied as CSS classes to the widget wrapper. These classes are sanitized using the ```sanitize_html_class()``` function built in to Wordpress.


### Automatically add paragraphs ###

Tick this box to automatically add paragraphs to any text. This uses the built in ```wpautop()``` function.

### Apply shortcodes ###

Tick this box to apply shortcodes to the text.

### Show the Title ###

Tick this box to show the widget title. If unchecked, the title (including the ```before_title``` and ```after_title``` text defined when registering the sidebar) will not be displayed.